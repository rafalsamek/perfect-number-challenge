<?php
namespace PerfectNumber;

use InvalidArgumentException;

/**
 * Class PerfectNumber
 * @package PerfectNumber
 */
class PerfectNumber
{
    const NUMBER_SHOULD_NOT_BE_EMPTY = 'Number should not be empty';
    const NUMBER_SHOULD_NOT_BE_NEGATIVE = 'Number should not be negative';
    const NUMBER_SHOULD_BE_INTEGER = 'Number should be integer';

    public static function getClassification($integer)
    {
        if(empty(trim($integer))) {
            throw new InvalidArgumentException(self::NUMBER_SHOULD_NOT_BE_EMPTY);
        }
        if($integer < 0) {
            throw new InvalidArgumentException(self::NUMBER_SHOULD_NOT_BE_NEGATIVE);
        }
        if(!is_int($integer)) {
            throw new InvalidArgumentException(self::NUMBER_SHOULD_BE_INTEGER);
        }

        $result = '';
        $aliquotes = self::findAliquotes($integer);
        $sumOfAliquotes = array_sum($aliquotes);
        if($integer == $sumOfAliquotes) {
            $result = 'perfect';
        } elseif($integer < $sumOfAliquotes) {
            $result = 'abundant';
        } elseif($integer > $sumOfAliquotes) {
            $result = 'deficient';
        }
        return $result;
    }

    private static function findAliquotes($integer)
    {
        $aliquotes = [];
        for($i = 1; $i < $integer; $i++) {
            if($integer % $i == 0) {
                $aliquotes[] = $i;
            }
        }
        return $aliquotes;
    }
}