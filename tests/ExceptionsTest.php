<?php

namespace PerfectNumber;

use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

/**
 * Class ExceptionsTest
 * @package PerfectNumber
 */
class ExceptionsTest extends TestCase
{
    const NUMBER_SHOULD_NOT_BE_EMPTY = 'Number should not be empty';
    const NUMBER_SHOULD_NOT_BE_NEGATIVE = 'Number should not be negative';
    const NUMBER_SHOULD_BE_INTEGER = 'Number should be integer';

    /**
     * @dataProvider emptyDataProvider
     * @param $input
     */
    public function testThrowExceptionWhenNumberIsEmpty($input)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(self::NUMBER_SHOULD_NOT_BE_EMPTY);
        PerfectNumber::getClassification($input);
    }

    public function emptyDataProvider()
    {
        return [
            [null],
            [false],
            [''],
            [' '],
            [0],
            ['0'],
            [0.0],
        ];
    }

    public function testThrowExceptionWhenNumberIsNegative()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(self::NUMBER_SHOULD_NOT_BE_NEGATIVE);
        PerfectNumber::getClassification(-1);
    }

    /**
     * @dataProvider nonIntegerDataProvider
     * @param $input
     */
    public function testThrowExceptionWhenNumberIsNotInteger($input)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(self::NUMBER_SHOULD_BE_INTEGER);
        PerfectNumber::getClassification($input);
    }

    public function nonIntegerDataProvider()
    {
        return [
            [5.5],
            ['lorem ipsum'],
        ];
    }
}