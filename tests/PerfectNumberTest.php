<?php

namespace PerfectNumber;

use PHPUnit\Framework\TestCase;

/**
 * Class PerfectNumberTest
 * @package PerfectNumber
 */
class PerfectNumberTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     *
     * @param int $input
     * @param string  $output
     */
    public function testNumber($input, $output)
    {
        $this->assertSame($output, PerfectNumber::getClassification($input));
    }

    public function dataProvider()
    {
        return [
            [1,'deficient'],
            [2,'deficient'],
            [3,'deficient'],
            [4,'deficient'],
            [5,'deficient'],
            [6,'perfect'],
            [7,'deficient'],
            [8,'deficient'],
            [9,'deficient'],
            [10,'deficient'],
            [11,'deficient'],
            [12,'abundant'],
            [13,'deficient'],
            [14,'deficient'],
            [15,'deficient'],
            [16,'deficient'],
            [17,'deficient'],
            [18,'abundant'],
            [19,'deficient'],
            [20,'abundant'],
            [21,'deficient'],
            [22,'deficient'],
            [23,'deficient'],
            [24,'abundant'],
            [25,'deficient'],
            [26,'deficient'],
            [27,'deficient'],
            [28,'perfect'],
            [29,'deficient'],
            [30,'abundant'],
            [31,'deficient'],
            [32,'deficient'],
            [33,'deficient'],
            [34,'deficient'],
            [35,'deficient'],
            [36,'abundant'],
            [37,'deficient'],
            [38,'deficient'],
            [39,'deficient'],
            [40,'abundant'],
            [41,'deficient'],
            [42,'abundant'],
            [43,'deficient'],
            [44,'deficient'],
            [45,'deficient'],
            [46,'deficient'],
            [47,'deficient'],
            [48,'abundant'],
            [49,'deficient'],
            [50,'deficient'],
            [496,'perfect'],
        ];
    }
}
